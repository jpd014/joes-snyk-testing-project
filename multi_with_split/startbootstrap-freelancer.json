{
  "vulnerabilities": [
    {
      "CVSSv3": "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N",
      "alternativeIds": [],
      "creationTime": "2019-02-15T11:59:02.487381Z",
      "credit": [
        "Yonatan Offek (poiu)"
      ],
      "cvssScore": 6.5,
      "description": "## Overview\n[bootstrap](https://www.npmjs.com/package/bootstrap) is a popular front-end framework for faster and easier web development.\n\nAffected versions of this package are vulnerable to Cross-site Scripting (XSS) in` data-template`, `data-content ` and `data-title` properties of tooltip/popover.\n## Details\n\nA cross-site scripting attack occurs when the attacker tricks a legitimate web-based application or site to accept a request as originating from a trusted source.\n\nThis is done by escaping the context of the web application; the web application then delivers that data to its users along with other trusted dynamic content, without validating it. The browser unknowingly executes malicious script on the client side (through client-side languages; usually JavaScript or HTML)  in order to perform actions that are otherwise typically blocked by the browserâ€™s Same Origin Policy.\n\nInjecting malicious code is the most prevalent manner by which XSS is exploited; for this reason, escaping characters in order to prevent this manipulation is the top method for securing code against this vulnerability.\n\nEscaping means that the application is coded to mark key characters, and particularly key characters included in user input, to prevent those characters from being interpreted in a dangerous context. For example, in HTML, `<` can be coded as  `&lt`; and `>` can be coded as `&gt`; in order to be interpreted and displayed as themselves in text, while within the code itself, they are used for HTML tags. If malicious content is injected into an application that escapes special characters and that malicious content uses `<` and `>` as HTML tags, those characters are nonetheless not interpreted as HTML tags by the browser if theyâ€™ve been correctly escaped in the application code and in this way the attempted attack is diverted.\n \nThe most prominent use of XSS is to steal cookies (source: OWASP HttpOnly) and hijack user sessions, but XSS exploits have been used to expose sensitive information, enable access to privileged services and functionality and deliver malware. \n\n### Types of attacks\nThere are a few methods by which XSS can be manipulated:\n\n|Type|Origin|Description|\n|--|--|--|\n|**Stored**|Server|The malicious code is inserted in the application (usually as a link) by the attacker. The code is activated every time a user clicks the link.|\n|**Reflected**|Server|The attacker delivers a malicious link externally from the vulnerable web site application to a user. When clicked, malicious code is sent to the vulnerable web site, which reflects the attack back to the userâ€™s browser.| \n|**DOM-based**|Client|The attacker forces the userâ€™s browser to render a malicious page. The data in the page itself delivers the cross-site scripting data.|\n|**Mutated**| |The attacker injects code that appears safe, but is then rewritten and modified by the browser, while parsing the markup. An example is rebalancing unclosed quotation marks or even adding quotation marks to unquoted parameters.|\n\n### Affected environments\nThe following environments are susceptible to an XSS attack:\n\n* Web servers\n* Application servers\n* Web application environments\n\n### How to prevent\nThis section describes the top best practices designed to specifically protect your code: \n\n* Sanitize data input in an HTTP request before reflecting it back, ensuring all data is validated, filtered or escaped before echoing anything back to the user, such as the values of query parameters during searches. \n* Convert special characters such as `?`, `&`, `/`, `<`, `>` and spaces to their respective HTML or URL encoded equivalents. \n* Give users the option to disable client-side scripts.\n* Redirect invalid requests.\n* Detect simultaneous logins, including those from two separate IP addresses, and invalidate those sessions.\n* Use and enforce a Content Security Policy (source: Wikipedia) to disable any features that might be manipulated for an XSS attack.\n* Read the documentation for any of the libraries referenced in your code to understand which elements allow for embedded HTML.\n\n## Remediation\nUpgrade `bootstrap` to version 3.4.1, 4.3.1 or higher.\n## References\n- [Bootstrap Blog](https://blog.getbootstrap.com/2019/02/13/bootstrap-4-3-1-and-3-4-1/)\n- [GitHub Commit](https://github.com/twbs/bootstrap/pull/28236/commits/5efa9b531d25927b907e3fa24b818608bc38a2f0)\n- [GitHub Commit](https://github.com/twbs/bootstrap-rubygem/commit/a63d04c96d14e42492ccdba1d7f3d6ec1af022a9)\n- [GitHub Issue](https://github.com/twbs/bootstrap/issues/28236)\n- [NPM Security Adviory](https://www.npmjs.com/advisories/891)\n",
      "disclosureTime": "2019-02-11T19:32:59Z",
      "exploit": "Not Defined",
      "fixedIn": [
        "3.4.1",
        "4.3.1"
      ],
      "functions": [],
      "functions_new": [],
      "id": "SNYK-JS-BOOTSTRAP-173700",
      "identifiers": {
        "CVE": [
          "CVE-2019-8331"
        ],
        "CWE": [
          "CWE-79"
        ],
        "GHSA": [
          "GHSA-9v3m-8fp8-mj99"
        ],
        "NSP": [
          891
        ]
      },
      "language": "js",
      "modificationTime": "2020-06-12T14:37:05.534360Z",
      "moduleName": "bootstrap",
      "packageManager": "npm",
      "packageName": "bootstrap",
      "patches": [],
      "proprietary": false,
      "publicationTime": "2019-02-15T19:32:59Z",
      "references": [
        {
          "title": "Bootstrap Blog",
          "url": "https://blog.getbootstrap.com/2019/02/13/bootstrap-4-3-1-and-3-4-1/"
        },
        {
          "title": "GitHub Commit",
          "url": "https://github.com/twbs/bootstrap/pull/28236/commits/5efa9b531d25927b907e3fa24b818608bc38a2f0"
        },
        {
          "title": "GitHub Commit",
          "url": "https://github.com/twbs/bootstrap-rubygem/commit/a63d04c96d14e42492ccdba1d7f3d6ec1af022a9"
        },
        {
          "title": "GitHub Issue",
          "url": "https://github.com/twbs/bootstrap/issues/28236"
        },
        {
          "title": "NPM Security Adviory",
          "url": "https://www.npmjs.com/advisories/891"
        }
      ],
      "semver": {
        "vulnerable": [
          "<3.4.1",
          ">=4.0.0 <4.3.1"
        ]
      },
      "severity": "medium",
      "severityWithCritical": "medium",
      "title": "Cross-site Scripting (XSS)",
      "from": [
        "startbootstrap-freelancer@5.0.2",
        "bootstrap@4.2.1"
      ],
      "upgradePath": [
        false,
        "bootstrap@4.3.1"
      ],
      "isUpgradable": true,
      "isPatchable": false,
      "name": "bootstrap",
      "version": "4.2.1"
    },
    {
      "CVSSv3": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:L/A:L/E:P",
      "alternativeIds": [],
      "creationTime": "2019-03-27T08:43:07.568451Z",
      "credit": [
        "Semmle Security Research Team"
      ],
      "cvssScore": 5.6,
      "description": "## Overview\n[jquery](https://www.npmjs.com/package/jquery) is a package that makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers.\n\nAffected versions of this package are vulnerable to Prototype Pollution. The `extend` function can be tricked into modifying the prototype of `Object` when the attacker controls part of the structure passed to this function. This can let an attacker add or modify an existing property that will then exist on all objects.\n\n## Details\n\nPrototype Pollution is a vulnerability affecting JavaScript. Prototype Pollution refers to the ability to inject properties into existing JavaScript language construct prototypes, such as objects. JavaScript allows all Object attributes to be altered, including their magical attributes such as `_proto_`, `constructor` and `prototype`. An attacker manipulates these attributes to overwrite, or pollute, a JavaScript application object prototype of the base object by injecting other values.  Properties on the `Object.prototype` are then inherited by all the JavaScript objects through the prototype chain. When that happens, this leads to either denial of service by triggering JavaScript exceptions, or it tampers with the application source code to force the code path that the attacker injects, thereby leading to remote code execution.\n\nThere are two main ways in which the pollution of prototypes occurs:\n\n-   Unsafe `Object` recursive merge\n    \n-   Property definition by path\n    \n\n### Unsafe Object recursive merge\n\nThe logic of a vulnerable recursive merge function follows the following high-level model:\n```\nmerge (target, source)\n\n  foreach property of source\n\n    if property exists and is an object on both the target and the source\n\n      merge(target[property], source[property])\n\n    else\n\n      target[property] = source[property]\n```\n<br>  \n\nWhen the source object contains a property named `_proto_` defined with `Object.defineProperty()` , the condition that checks if the property exists and is an object on both the target and the source passes and the merge recurses with the target, being the prototype of `Object` and the source of `Object` as defined by the attacker. Properties are then copied on the `Object` prototype.\n\nClone operations are a special sub-class of unsafe recursive merges, which occur when a recursive merge is conducted on an empty object: `merge({},source)`.\n\n`lodash` and `Hoek` are examples of libraries susceptible to recursive merge attacks.\n\n### Property definition by path\n\nThere are a few JavaScript libraries that use an API to define property values on an object based on a given path. The function that is generally affected contains this signature: `theFunction(object, path, value)`\n\nIf the attacker can control the value of â€œpathâ€, they can set this value to `_proto_.myValue`. `myValue` is then assigned to the prototype of the class of the object.\n\n## Types of attacks\n\nThere are a few methods by which Prototype Pollution can be manipulated:\n\n| Type |Origin  |Short description |\n|--|--|--|\n| **Denial of service (DoS)**|Client  |This is the most likely attack. <br>DoS occurs when `Object` holds generic functions that are implicitly called for various operations (for example, `toString` and `valueOf`). <br> The attacker pollutes `Object.prototype.someattr` and alters its state to an unexpected value such as `Int` or `Object`. In this case, the code fails and is likely to cause a denial of service.  <br>**For example:** if an attacker pollutes `Object.prototype.toString` by defining it as an integer, if the codebase at any point was reliant on `someobject.toString()` it would fail. |\n |**Remote Code Execution**|Client|Remote code execution is generally only possible in cases where the codebase evaluates a specific attribute of an object, and then executes that evaluation.<br>**For example:** `eval(someobject.someattr)`. In this case, if the attacker pollutes `Object.prototype.someattr` they are likely to be able to leverage this in order to execute code.|\n|**Property Injection**|Client|The attacker pollutes properties that the codebase relies on for their informative value, including security properties such as cookies or tokens.<br>  **For example:** if a codebase checks privileges for `someuser.isAdmin`, then when the attacker pollutes `Object.prototype.isAdmin` and sets it to equal `true`, they can then achieve admin privileges.|\n\n## Affected environments\n\nThe following environments are susceptible to a Prototype Pollution attack:\n\n-   Application server\n    \n-   Web server\n    \n\n## How to prevent\n\n1.  Freeze the prototypeâ€” use `Object.freeze (Object.prototype)`.\n    \n2.  Require schema validation of JSON input.\n    \n3.  Avoid using unsafe recursive merge functions.\n    \n4.  Consider using objects without prototypes (for example, `Object.create(null)`), breaking the prototype chain and preventing pollution.\n    \n5.  As a best practice use `Map` instead of `Object`.\n\n### For more information on this vulnerability type:\n\n[Arteau, Oliver. â€œJavaScript prototype pollution attack in NodeJS application.â€ GitHub, 26 May 2018](https://github.com/HoLyVieR/prototype-pollution-nsec18/blob/master/paper/JavaScript_prototype_pollution_attack_in_NodeJS.pdf)\n\n## Remediation\nUpgrade `jquery` to version 3.4.0 or higher.\n## References\n- [GitHub Commit](https://github.com/jquery/jquery/commit/753d591aea698e57d6db58c9f722cd0808619b1b)\n- [GitHub PR](https://github.com/jquery/jquery/pull/4333)\n- [Hackerone Report](https://hackerone.com/reports/454365)\n- [Snyk Blog](https://snyk.io/blog/after-three-years-of-silence-a-new-jquery-prototype-pollution-vulnerability-emerges-once-again/)\n- [Third-Party Backported Patches Repo](https://github.com/DanielRuf/snyk-js-jquery-174006)\n",
      "disclosureTime": "2019-03-26T08:40:15Z",
      "exploit": "Proof of Concept",
      "fixedIn": [
        "3.4.0"
      ],
      "functions": [
        {
          "functionId": {
            "className": null,
            "filePath": "test/core.js",
            "functionName": "module.exports.jQuery.extend(Object, Object)"
          },
          "version": [
            "<=1.8.3"
          ]
        },
        {
          "functionId": {
            "className": null,
            "filePath": "src/core.js",
            "functionName": "jQuery.extend.jQuery.fn.extend"
          },
          "version": [
            ">1.8.3 <=2.2.4"
          ]
        },
        {
          "functionId": {
            "className": null,
            "filePath": "dist/core.js",
            "functionName": "jQuery.extend.jQuery.fn.extend"
          },
          "version": [
            ">2.2.4 <=3.3.1"
          ]
        }
      ],
      "functions_new": [
        {
          "functionId": {
            "filePath": "test/core.js",
            "functionName": "module.exports.jQuery.extend(Object, Object)"
          },
          "version": [
            "<=1.8.3"
          ]
        },
        {
          "functionId": {
            "filePath": "src/core.js",
            "functionName": "jQuery.extend.jQuery.fn.extend"
          },
          "version": [
            ">1.8.3 <=2.2.4"
          ]
        },
        {
          "functionId": {
            "filePath": "dist/core.js",
            "functionName": "jQuery.extend.jQuery.fn.extend"
          },
          "version": [
            ">2.2.4 <=3.3.1"
          ]
        }
      ],
      "id": "SNYK-JS-JQUERY-174006",
      "identifiers": {
        "CVE": [
          "CVE-2019-11358",
          "CVE-2019-5428"
        ],
        "CWE": [
          "CWE-400"
        ],
        "GHSA": [
          "GHSA-wv67-q8rr-grjp"
        ],
        "NSP": [
          796
        ]
      },
      "language": "js",
      "modificationTime": "2021-01-07T17:30:33.860835Z",
      "moduleName": "jquery",
      "packageManager": "npm",
      "packageName": "jquery",
      "patches": [],
      "proprietary": false,
      "publicationTime": "2019-03-27T08:40:08Z",
      "references": [
        {
          "title": "GitHub Commit",
          "url": "https://github.com/jquery/jquery/commit/753d591aea698e57d6db58c9f722cd0808619b1b"
        },
        {
          "title": "GitHub PR",
          "url": "https://github.com/jquery/jquery/pull/4333"
        },
        {
          "title": "Hackerone Report",
          "url": "https://hackerone.com/reports/454365"
        },
        {
          "title": "Snyk Blog",
          "url": "https://snyk.io/blog/after-three-years-of-silence-a-new-jquery-prototype-pollution-vulnerability-emerges-once-again/"
        },
        {
          "title": "Third-Party Backported Patches Repo",
          "url": "https://github.com/DanielRuf/snyk-js-jquery-174006"
        }
      ],
      "semver": {
        "vulnerable": [
          "<3.4.0"
        ]
      },
      "severity": "medium",
      "severityWithCritical": "medium",
      "title": "Prototype Pollution",
      "from": [
        "startbootstrap-freelancer@5.0.2",
        "jquery@3.3.1"
      ],
      "upgradePath": [
        false,
        "jquery@3.4.0"
      ],
      "isUpgradable": true,
      "isPatchable": false,
      "name": "jquery",
      "version": "3.3.1"
    },
    {
      "CVSSv3": "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C",
      "alternativeIds": [],
      "creationTime": "2020-04-13T07:16:49.518552Z",
      "credit": [
        "Masato Kinugawa"
      ],
      "cvssScore": 6.3,
      "description": "## Overview\n\n[jquery](https://www.npmjs.com/package/jquery) is a package that makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers.\n\n\nAffected versions of this package are vulnerable to Cross-site Scripting (XSS)\nPassing HTML containing `<option>` elements from untrusted sources - even after sanitizing it - to one of jQuery's DOM manipulation methods (i.e. `.html()`, `.append()`, and others) may execute untrusted code.\n\n## Details\nA cross-site scripting attack occurs when the attacker tricks a legitimate web-based application or site to accept a request as originating from a trusted source.\r\n\r\nThis is done by escaping the context of the web application; the web application then delivers that data to its users along with other trusted dynamic content, without validating it. The browser unknowingly executes malicious script on the client side (through client-side languages; usually JavaScript or HTML)  in order to perform actions that are otherwise typically blocked by the browserâ€™s Same Origin Policy.\r\n\r\nÖ¿Injecting malicious code is the most prevalent manner by which XSS is exploited; for this reason, escaping characters in order to prevent this manipulation is the top method for securing code against this vulnerability.\r\n\r\nEscaping means that the application is coded to mark key characters, and particularly key characters included in user input, to prevent those characters from being interpreted in a dangerous context. For example, in HTML, `<` can be coded as  `&lt`; and `>` can be coded as `&gt`; in order to be interpreted and displayed as themselves in text, while within the code itself, they are used for HTML tags. If malicious content is injected into an application that escapes special characters and that malicious content uses `<` and `>` as HTML tags, those characters are nonetheless not interpreted as HTML tags by the browser if theyâ€™ve been correctly escaped in the application code and in this way the attempted attack is diverted.\r\n \r\nThe most prominent use of XSS is to steal cookies (source: OWASP HttpOnly) and hijack user sessions, but XSS exploits have been used to expose sensitive information, enable access to privileged services and functionality and deliver malware. \r\n\r\n### Types of attacks\r\nThere are a few methods by which XSS can be manipulated:\r\n\r\n|Type|Origin|Description|\r\n|--|--|--|\r\n|**Stored**|Server|The malicious code is inserted in the application (usually as a link) by the attacker. The code is activated every time a user clicks the link.|\r\n|**Reflected**|Server|The attacker delivers a malicious link externally from the vulnerable web site application to a user. When clicked, malicious code is sent to the vulnerable web site, which reflects the attack back to the userâ€™s browser.| \r\n|**DOM-based**|Client|The attacker forces the userâ€™s browser to render a malicious page. The data in the page itself delivers the cross-site scripting data.|\r\n|**Mutated**| |The attacker injects code that appears safe, but is then rewritten and modified by the browser, while parsing the markup. An example is rebalancing unclosed quotation marks or even adding quotation marks to unquoted parameters.|\r\n\r\n### Affected environments\r\nThe following environments are susceptible to an XSS attack:\r\n\r\n* Web servers\r\n* Application servers\r\n* Web application environments\r\n\r\n### How to prevent\r\nThis section describes the top best practices designed to specifically protect your code: \r\n\r\n* Sanitize data input in an HTTP request before reflecting it back, ensuring all data is validated, filtered or escaped before echoing anything back to the user, such as the values of query parameters during searches. \r\n* Convert special characters such as `?`, `&`, `/`, `<`, `>` and spaces to their respective HTML or URL encoded equivalents. \r\n* Give users the option to disable client-side scripts.\r\n* Redirect invalid requests.\r\n* Detect simultaneous logins, including those from two separate IP addresses, and invalidate those sessions.\r\n* Use and enforce a Content Security Policy (source: Wikipedia) to disable any features that might be manipulated for an XSS attack.\r\n* Read the documentation for any of the libraries referenced in your code to understand which elements allow for embedded HTML.\n\n## Remediation\n\nUpgrade `jquery` to version 3.5.0 or higher.\n\n\n## References\n\n- [GitHub Commit](https://github.com/jquery/jquery/commit/1d61fd9407e6fbe82fe55cb0b938307aa0791f77)\n\n- [PoC](https://vulnerabledoma.in/jquery_htmlPrefilter_xss.html)\n\n- [Release Notes](https://blog.jquery.com/2020/04/10/jquery-3-5-0-released/)\n\n- [Security Blog](https://masatokinugawa.l0.cm/2020/05/jquery3.5.0-xss.html?spref=tw)\n",
      "disclosureTime": "2020-04-10T00:00:00Z",
      "exploit": "Proof of Concept",
      "fixedIn": [
        "3.5.0"
      ],
      "functions": [
        {
          "functionId": {
            "className": null,
            "filePath": "src/manipulation.js",
            "functionName": "htmlPrefilter"
          },
          "version": [
            ">=1.0.3 <3.5.0"
          ]
        }
      ],
      "functions_new": [
        {
          "functionId": {
            "filePath": "src/manipulation.js",
            "functionName": "htmlPrefilter"
          },
          "version": [
            ">=1.0.3 <3.5.0"
          ]
        }
      ],
      "id": "SNYK-JS-JQUERY-565129",
      "identifiers": {
        "CVE": [
          "CVE-2020-11023"
        ],
        "CWE": [
          "CWE-79"
        ],
        "GHSA": [
          "GHSA-jpcq-cgw6-v4j6"
        ]
      },
      "language": "js",
      "modificationTime": "2020-05-11T07:50:37.649500Z",
      "moduleName": "jquery",
      "packageManager": "npm",
      "packageName": "jquery",
      "patches": [],
      "proprietary": false,
      "publicationTime": "2020-04-13T15:33:49Z",
      "references": [
        {
          "title": "GitHub Commit",
          "url": "https://github.com/jquery/jquery/commit/1d61fd9407e6fbe82fe55cb0b938307aa0791f77"
        },
        {
          "title": "PoC",
          "url": "https://vulnerabledoma.in/jquery_htmlPrefilter_xss.html"
        },
        {
          "title": "Release Notes",
          "url": "https://blog.jquery.com/2020/04/10/jquery-3-5-0-released/"
        },
        {
          "title": "Security Blog",
          "url": "https://masatokinugawa.l0.cm/2020/05/jquery3.5.0-xss.html?spref=tw"
        }
      ],
      "semver": {
        "vulnerable": [
          ">=1.0.3 <3.5.0"
        ]
      },
      "severity": "medium",
      "severityWithCritical": "medium",
      "title": "Cross-site Scripting (XSS)",
      "from": [
        "startbootstrap-freelancer@5.0.2",
        "jquery@3.3.1"
      ],
      "upgradePath": [
        false,
        "jquery@3.5.0"
      ],
      "isUpgradable": true,
      "isPatchable": false,
      "name": "jquery",
      "version": "3.3.1"
    },
    {
      "CVSSv3": "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N/E:P/RL:O/RC:R",
      "alternativeIds": [],
      "creationTime": "2020-04-30T12:29:39.885866Z",
      "credit": [
        "Masato Kinugawa"
      ],
      "cvssScore": 6.5,
      "description": "## Overview\n\n[jquery](https://www.npmjs.com/package/jquery) is a package that makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers.\n\n\nAffected versions of this package are vulnerable to Cross-site Scripting (XSS).\nPassing HTML from untrusted sources - even after sanitizing it - to one of jQuery's DOM manipulation methods (i.e. `.html(), .append()`, and others) may execute untrusted code.\n\n\n## Details:\nA cross-site scripting attack occurs when the attacker tricks a legitimate web-based application or site to accept a request as originating from a trusted source.\r\n\r\nThis is done by escaping the context of the web application; the web application then delivers that data to its users along with other trusted dynamic content, without validating it. The browser unknowingly executes malicious script on the client side (through client-side languages; usually JavaScript or HTML)  in order to perform actions that are otherwise typically blocked by the browserâ€™s Same Origin Policy.\r\n\r\nÖ¿Injecting malicious code is the most prevalent manner by which XSS is exploited; for this reason, escaping characters in order to prevent this manipulation is the top method for securing code against this vulnerability.\r\n\r\nEscaping means that the application is coded to mark key characters, and particularly key characters included in user input, to prevent those characters from being interpreted in a dangerous context. For example, in HTML, `<` can be coded as  `&lt`; and `>` can be coded as `&gt`; in order to be interpreted and displayed as themselves in text, while within the code itself, they are used for HTML tags. If malicious content is injected into an application that escapes special characters and that malicious content uses `<` and `>` as HTML tags, those characters are nonetheless not interpreted as HTML tags by the browser if theyâ€™ve been correctly escaped in the application code and in this way the attempted attack is diverted.\r\n \r\nThe most prominent use of XSS is to steal cookies (source: OWASP HttpOnly) and hijack user sessions, but XSS exploits have been used to expose sensitive information, enable access to privileged services and functionality and deliver malware. \r\n\r\n### Types of attacks\r\nThere are a few methods by which XSS can be manipulated:\r\n\r\n|Type|Origin|Description|\r\n|--|--|--|\r\n|**Stored**|Server|The malicious code is inserted in the application (usually as a link) by the attacker. The code is activated every time a user clicks the link.|\r\n|**Reflected**|Server|The attacker delivers a malicious link externally from the vulnerable web site application to a user. When clicked, malicious code is sent to the vulnerable web site, which reflects the attack back to the userâ€™s browser.| \r\n|**DOM-based**|Client|The attacker forces the userâ€™s browser to render a malicious page. The data in the page itself delivers the cross-site scripting data.|\r\n|**Mutated**| |The attacker injects code that appears safe, but is then rewritten and modified by the browser, while parsing the markup. An example is rebalancing unclosed quotation marks or even adding quotation marks to unquoted parameters.|\r\n\r\n### Affected environments\r\nThe following environments are susceptible to an XSS attack:\r\n\r\n* Web servers\r\n* Application servers\r\n* Web application environments\r\n\r\n### How to prevent\r\nThis section describes the top best practices designed to specifically protect your code: \r\n\r\n* Sanitize data input in an HTTP request before reflecting it back, ensuring all data is validated, filtered or escaped before echoing anything back to the user, such as the values of query parameters during searches. \r\n* Convert special characters such as `?`, `&`, `/`, `<`, `>` and spaces to their respective HTML or URL encoded equivalents. \r\n* Give users the option to disable client-side scripts.\r\n* Redirect invalid requests.\r\n* Detect simultaneous logins, including those from two separate IP addresses, and invalidate those sessions.\r\n* Use and enforce a Content Security Policy (source: Wikipedia) to disable any features that might be manipulated for an XSS attack.\r\n* Read the documentation for any of the libraries referenced in your code to understand which elements allow for embedded HTML.\n\n\n## Remediation\n\nUpgrade `jquery` to version 3.5.0 or higher.\n\n\n## References\n\n- [GHSA](https://github.com/jquery/jquery/security/advisories/GHSA-gxr4-xjj5-5px2)\n\n- [GitHub Commit](https://github.com/jquery/jquery/commit/1d61fd9407e6fbe82fe55cb0b938307aa0791f77)\n\n- [JQuery 3.5.0 Release](https://blog.jquery.com/2020/04/10/jquery-3-5-0-released/)\n\n- [JQuery Upgrade Guide](https://jquery.com/upgrade-guide/3.5/)\n\n- [PoC](https://vulnerabledoma.in/jquery_htmlPrefilter_xss.html)\n\n- [Security Blog](https://mksben.l0.cm/2020/05/jquery3.5.0-xss.html)\n",
      "disclosureTime": "2020-04-29T23:02:09Z",
      "exploit": "Proof of Concept",
      "fixedIn": [
        "3.5.0"
      ],
      "functions": [],
      "functions_new": [],
      "id": "SNYK-JS-JQUERY-567880",
      "identifiers": {
        "CVE": [
          "CVE-2020-11022"
        ],
        "CWE": [
          "CWE-79"
        ],
        "GHSA": [
          "GHSA-v73w-r9xg-7cr9"
        ],
        "NSP": [
          1518
        ]
      },
      "language": "js",
      "modificationTime": "2020-05-05T06:44:17.559695Z",
      "moduleName": "jquery",
      "packageManager": "npm",
      "packageName": "jquery",
      "patches": [],
      "proprietary": false,
      "publicationTime": "2020-04-29T23:02:09Z",
      "references": [
        {
          "title": "GHSA",
          "url": "https://github.com/jquery/jquery/security/advisories/GHSA-gxr4-xjj5-5px2"
        },
        {
          "title": "GitHub Commit",
          "url": "https://github.com/jquery/jquery/commit/1d61fd9407e6fbe82fe55cb0b938307aa0791f77"
        },
        {
          "title": "JQuery 3.5.0 Release",
          "url": "https://blog.jquery.com/2020/04/10/jquery-3-5-0-released/"
        },
        {
          "title": "JQuery Upgrade Guide",
          "url": "https://jquery.com/upgrade-guide/3.5/"
        },
        {
          "title": "PoC",
          "url": "https://vulnerabledoma.in/jquery_htmlPrefilter_xss.html"
        },
        {
          "title": "Security Blog",
          "url": "https://mksben.l0.cm/2020/05/jquery3.5.0-xss.html"
        }
      ],
      "semver": {
        "vulnerable": [
          ">=1.2.0 <3.5.0"
        ]
      },
      "severity": "medium",
      "severityWithCritical": "medium",
      "title": "Cross-site Scripting (XSS)",
      "from": [
        "startbootstrap-freelancer@5.0.2",
        "jquery@3.3.1"
      ],
      "upgradePath": [
        false,
        "jquery@3.5.0"
      ],
      "isUpgradable": true,
      "isPatchable": false,
      "name": "jquery",
      "version": "3.3.1"
    }
  ],
  "ok": false,
  "dependencyCount": 5,
  "org": "amfam-testing",
  "policy": "# Snyk (https://snyk.io) policy file, patches or ignores known vulnerabilities.\nversion: v1.19.0\nignore: {}\npatch: {}\n",
  "isPrivate": true,
  "licensesPolicy": {
    "severities": {},
    "orgLicenseRules": {
      "AGPL-1.0": {
        "licenseType": "AGPL-1.0",
        "severity": "high",
        "instructions": ""
      },
      "AGPL-3.0": {
        "licenseType": "AGPL-3.0",
        "severity": "high",
        "instructions": ""
      },
      "Artistic-1.0": {
        "licenseType": "Artistic-1.0",
        "severity": "medium",
        "instructions": ""
      },
      "Artistic-2.0": {
        "licenseType": "Artistic-2.0",
        "severity": "medium",
        "instructions": ""
      },
      "CDDL-1.0": {
        "licenseType": "CDDL-1.0",
        "severity": "medium",
        "instructions": ""
      },
      "CPOL-1.02": {
        "licenseType": "CPOL-1.02",
        "severity": "high",
        "instructions": ""
      },
      "EPL-1.0": {
        "licenseType": "EPL-1.0",
        "severity": "medium",
        "instructions": ""
      },
      "GPL-2.0": {
        "licenseType": "GPL-2.0",
        "severity": "high",
        "instructions": ""
      },
      "GPL-3.0": {
        "licenseType": "GPL-3.0",
        "severity": "high",
        "instructions": ""
      },
      "LGPL-2.0": {
        "licenseType": "LGPL-2.0",
        "severity": "medium",
        "instructions": ""
      },
      "LGPL-2.1": {
        "licenseType": "LGPL-2.1",
        "severity": "medium",
        "instructions": ""
      },
      "LGPL-3.0": {
        "licenseType": "LGPL-3.0",
        "severity": "medium",
        "instructions": ""
      },
      "MPL-1.1": {
        "licenseType": "MPL-1.1",
        "severity": "medium",
        "instructions": ""
      },
      "MPL-2.0": {
        "licenseType": "MPL-2.0",
        "severity": "medium",
        "instructions": ""
      },
      "MS-RL": {
        "licenseType": "MS-RL",
        "severity": "medium",
        "instructions": ""
      },
      "SimPL-2.0": {
        "licenseType": "SimPL-2.0",
        "severity": "high",
        "instructions": ""
      }
    }
  },
  "packageManager": "npm",
  "projectId": "1c74acff-8b62-45e6-88b7-c77a43a92751",
  "ignoreSettings": null,
  "summary": "4 vulnerable dependency paths",
  "remediation": {
    "unresolved": [],
    "upgrade": {
      "bootstrap@4.2.1": {
        "upgradeTo": "bootstrap@4.3.1",
        "upgrades": [
          "bootstrap@4.2.1"
        ],
        "vulns": [
          "SNYK-JS-BOOTSTRAP-173700"
        ]
      },
      "jquery@3.3.1": {
        "upgradeTo": "jquery@3.5.0",
        "upgrades": [
          "jquery@3.3.1",
          "jquery@3.3.1",
          "jquery@3.3.1"
        ],
        "vulns": [
          "SNYK-JS-JQUERY-565129",
          "SNYK-JS-JQUERY-567880",
          "SNYK-JS-JQUERY-174006"
        ]
      }
    },
    "patch": {},
    "ignore": {},
    "pin": {}
  },
  "filesystemPolicy": false,
  "filtered": {
    "ignore": [],
    "patch": []
  },
  "uniqueCount": 4,
  "projectName": "startbootstrap-freelancer",
  "foundProjectCount": 35,
  "displayTargetFile": "docs/package-lock.json",
  "path": "/builds/amfament/ent/tanuki/snyk-sca/test-projects/webgoat_test"
}
