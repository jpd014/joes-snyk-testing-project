#!/bin/bash
#set -xv
########################################################################
# (c) Copyright 2020, ZeroNorth, Inc., support@zeronorth.io
#
# Script to read in a Snyk multi-project scan results JSON file and then
# to output 1 JSON file per project for uploading to ZeroNorth.
#
# Requires: curl, jq
#
########################################################################
#
# Before using this script, obtain your API key using the instructions
# outlined in the following KB article:
#
#   https://support.zeronorth.io/hc/en-us/articles/115003679033
#
# The API key can then be used in one of the following ways:
# 1) Stored in secure file and then referenced at run time.
# 2) Set as the value to the environment variable API_KEY.
# 3) Set as the value to the variable API_KEY within this script.
#
# IMPORTANT: An API key generated using the above method has life span
# of 10 calendar years.
#
########################################################################
# The below variable is used to filter out undesirable projects from the
# input file. The value to the variable must be a pattern string suitable
# for egrep -v.
########################################################################
EXC_PATTERN='\.Test'

MY_NAME=`basename $0`
MY_DIR=`dirname $0`


########################################################################
# Function to print time-stamped messages
########################################################################
function print_msg {
    echo "`date +'%Y-%m-%d %T'`  $1" >&2
}


########################################################################
# Print the help info.
########################################################################
if [ ! "$1" ]
then
    echo "
Script to read in a Snyk multi-project scan results JSON file and then
to output 1 JSON file per project for uploading to ZeroNorth. Be sure to
examine and adjust the EXC_PATTERN variable inside script as needed.


Usage: `basename $0` <input file> [<output directory>]

Examples:

  `basename $0` my_snyk_file.json
  `basename $0` my_snyk_file.json /tmp

where,

  <input file>         - The path to the input JSON file. The input JSON
                         file must be a multi-project, Snyk scan results
                         file.

  [<output directory>] - Optional. The directory to write the output
                         files to. If not provided, the output files will
                         be written to the current directory.
" >&2
    exit 1
fi

# Read in the Target ID.
IN_FILE="$1"; shift
print_msg "Input file is '${IN_FILE}'."

# Set or read in the output directory.
OUT_DIR='.'
[ "$1" ] && OUT_DIR="$1"
print_msg "Output directory is '${OUT_DIR}'."


########################################################################
# First, obtain the list of the projects from the input file, excluding
# those that are deemed not desirable based on the pattern specified in
# the variable EXC_PATTERN (see above).
########################################################################
projects=$(cat "$IN_FILE" | jq -r '.[].projectName' | egrep -v "$EXC_PATTERN" | sort)
p_count=$(echo -n "$projects" | wc -l)
print_msg "Found $p_count qualifying projects."


########################################################################
# Iterate through the projects list, extracting the JSON results for
# each project and then writing them out to the output files.
########################################################################
while read p_name
do
    out_file=${OUT_DIR}/${p_name}.json
    touch ${out_file}

    if [ $? -ne 0 ]; then
        print_msg "ERROR: unable to write the output file '${out_file}'. Exiting."
        exit 1
    fi

    cat "$IN_FILE" | jq '.[] | select(.projectName == "'"$p_name"'")' > ${out_file}
    [ $? -eq 0 ] && print_msg "Wrote '${out_file}'."
done <<< "$projects"  # The Projects list from above.


########################################################################
# The End
########################################################################
print_msg "DONE."
